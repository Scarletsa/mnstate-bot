var name = require("./name");

module.exports = function (req, res) {
  if (req.body.result.action === "bot.name") {
    return name(req, res);
  } 
}