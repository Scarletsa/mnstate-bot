// server.js
// where your node app starts

// init project
var express = require('express');
var app = express();

// Connect to database via MONGODB_URI connection string defined in .env
var db = require('monk')(process.env.MONGODB_URI);
// Generate monk object for 'users' collection
var users = db.get('users');

var bodyParser = require('body-parser'); // Express "middleware" that processes json bodies in HTTP requests
var botResponses = require('./responses/bot'); // Custom code for handling messages related to bots
var request = require('request'); // A library for making HTTP requests to outside services

// http://expressjs.com/en/starter/static-files.html
// Expose terms and privacy policy
app.use(express.static('public'));
app.use(bodyParser.json()); // Have express use bodyParser to process json bodies


app.post("/apiai", function (req, res) {
  if (req.body.result.action.includes("bot")) {
    return botResponses(req, res);
  }
});


// http://expressjs.com/en/starter/basic-routing.html
app.get("/", function (request, response) {
  response.sendFile(__dirname + '/views/index.html');
});

app.get("/dreams", function (request, response) {
  response.send(dreams);
});

// could also use the POST body instead of query string: http://expressjs.com/en/api.html#req.body
app.post("/dreams", function (request, response) {
  dreams.push(request.query.dream);
  response.sendStatus(200);
});

// Simple in-memory store for now
var dreams = [
  "Find and count some sheep",
  "Climb a really tall mountain",
  "Wash the dishes"
];

// listen for requests :)
var listener = app.listen(process.env.PORT, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});
